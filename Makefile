CC = gcc
CFLAGS += -Wall -Wextra

.PHONY: all
all: gcc g++

.PHONY: gcc
gcc: isdigit_gcc \
	 isdigit_gcc_O0 \
	 isdigit_gcc_O1 \
	 isdigit_gcc_O2 \
	 isdigit_gcc_O3 \
	 isdigit_gcc_O2_no-builtin \
	 isdigit_gcc_O2_no-inline \
	 isdigit_gcc_O2_no-builtin_no-inline

.PHONY: g++
g++: isdigit_g++ \
	 isdigit_g++_O0 \
	 isdigit_g++_O1 \
	 isdigit_g++_O2 \
	 isdigit_g++_O3 \
	 isdigit_g++_O2_no-builtin \
	 isdigit_g++_O2_no-inline \
	 isdigit_g++_O2_no-builtin_no-inline

isdigit_g+%: CC = g++
isdigit_%_O0: CFLAGS += -O0
isdigit_%_O1: CFLAGS += -O1
isdigit_%_O2: CFLAGS += -O2
isdigit_%_O3: CFLAGS += -O3
isdigit_%_O2_no-builtin: CFLAGS += -O2 -fno-builtin
isdigit_%_O2_no-inline:  CFLAGS += -O2 -fno-inline
isdigit_%_O2_no-builtin_no-inline: CFLAGS += -O2 -fno-builtin -fno-inline

% : isdigit.c
	$(CC) -o $@ $^ $(CFLAGS)

nm: gcc g++
	nm -D isdigit_*

.PHONY: clean
clean:
	$(RM) $(wildcard isdigit_*)
